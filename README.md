![image](public/img/snip_my-blog.png)

### Blogul meu personal
**Important!** This repository it's raw and need of complex changes.

* [Weird Docs](docs/DOC.md)
* [Questions](docs/QUESTIONS.md)
* [Tasks](docs/TASKS.md)

### Extern Links :
* [Free CSS](https://www.free-css.com/free-css-templates/page255/devblog-v1.1)
* [Google Web Creators](https://support.google.com/blogger/thread/84172078/new-join-the-community-of-google-web-creators?hl=en&authuser=0)
* [gatsbyjs.com/](https://www.gatsbyjs.com/)
