let router = require("express").Router();
let path = require("path"); 

// 
// router.get("/", function(req, res) {
// 	res.sendFile(path.join(__dirname, 'public', 'my-blog.html'));
// });

router.get("/post_number", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/post.html'));
});

// Bellow is Routes from left-menu, main-page. 
router.get("/SEE_HISTORY", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/see_history.html'));
});

router.get("/SUBSCRIPTIONS", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/subscriptions.html'));
});

router.get("/LIKED_POST", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/liked_post.html'));
});

router.get("/NOTIFICATIONS", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/notifications.html'));
});

router.get("/TAGS", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/tags.html'));
});

router.get("/AUTHORS", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/authors.html'));
});

router.get("/DATE", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/date.html'));
});

router.get("/NEWEST", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/newest.html'));
});

router.get("/OLDEST", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/oldest.html'));
});

router.get("/VIEWS", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/views.html'));
});

router.get("/STARS", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/stars.html'));
});

router.get("/ALPHABETIC", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/alphabetic.html'));
});

router.get("/YOUR_POST", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/your_post.html'));
});

module.exports = router;