let router = require("express").Router();
let path = require("path"); 

router.get("/sign_in", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/at-head/reg.html'));
});

router.get("/sign_up", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/at-head/reg.html'));
});

module.exports = router;