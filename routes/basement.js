let basement = require("express").Router();
let path = require("path"); 

// Bellow is: Basement Links :
basement.get("/languages", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/basement/languages.html'));
});

basement.get("/FAQ", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/basement/FAQ.html'));
});

basement.get("/settings", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/basement/settings.html'));
});

basement.get("/CONTACTS", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/contacts.html'));
});

module.exports = basement;