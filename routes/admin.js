let adm = require("express").Router();
let path = require("path"); 

// Bellow is routes at - Admin: 
adm.get("/connect_to_admin_panel", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/admin/connect-admin.html'));
});

adm.get("/admin_panel", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/admin/admin-page.html'));
});

module.exports = adm;