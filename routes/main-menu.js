let main_menu = require("express").Router();
let path = require("path"); 

main_menu.get("/forum", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/main-menu/forum.html'));
});

main_menu.get("/categories", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/main-menu/categories.html'));
});

main_menu.get("/NEWS", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/main-menu/news.html'));
});

main_menu.get("/other", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', 'html/main-menu/other.html'));
});

module.exports = main_menu;

