const express = require("express");
const app = express();
const path = require("path");
const PORT = 12;
const router = require("./routes/head.js");
const admin = require("./routes/admin.js");
const pug = require("pug");

// Middleware :
app.use("/", router);
app.use(express.static("public"));

// routes
app.get("/", function(req, res) {
	res.sendFile(__dirname + '/my-blog.html');
});

// config
app.set('view engine', 'pug');

// PORT LISTENING ...
app.listen(PORT, function() {
	console.log("Server running port : " + PORT);
});